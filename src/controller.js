function Controller() {
    var $ctrl = this;

    $ctrl.filter = {};
    $ctrl.id = 0;

    $ctrl.$onInit = $onInit;
    $ctrl.$onChange = $onChange;
    $ctrl.deleteRow = deleteRow;
    $ctrl.addRow = addRow;
    $ctrl.getCount = getCount;

    function $onInit() {
        $ctrl.storage = JSON.parse(localStorage.getItem('sml')) || [];

        if ($ctrl.storage.length) {
            $ctrl.id = $ctrl.storage[$ctrl.storage.length - 1].id;
        }
    }

    function $onChange() {
        localStorage.setItem('sml', JSON.stringify($ctrl.storage));
    }

    function getCount() {
        var count = 0;

        angular.forEach($ctrl.storage, function (item) {
            count += item.guest.coupled ? 2 : 1;
        });

        return count;
    }

    function addRow(filter) {
        //Значения по умолчанию
        $ctrl.storage.push({
            id: ++$ctrl.id,
            guest: {
                name: '',
                age: 25,
                coupled: false,
                drink: filter || 'WINE'
            }
        });

        $ctrl.$onChange();
    }

    function deleteRow(id) {
        var guestIndex = $ctrl.storage.findIndex(function (guest) {
            return guest.id === id;
        });

        $ctrl.storage.splice(guestIndex, 1);

        $ctrl.$onChange();
    }
}

export default Controller;