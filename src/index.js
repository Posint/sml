import Controller from './controller';

angular.module('sml', [])
    .filter('drink', function () {
        return function (array, drink) {
            var out = array;

            if (drink) {
                out = [];

                angular.forEach(array, function (item) {
                    item.guest.drink === drink && this.push(item);
                }, out);
            }

            return out;
        };
    })
    .controller('smlCtrl', Controller);
