'use strict';

let webpack = require('webpack');
let path    = require('path');
let html    = require('html-webpack-plugin');
let live    = require('webpack-livereload-plugin');
let hot     = require('webpack/lib/HotModuleReplacementPlugin');

module.exports = {
  entry: {
    index: './src/index.js',
    vendor: 'angular'
  },
  output: {
    path: path.resolve(__dirname, 'dict'),
    filename: 'js/index.js'
  },
  resolve: {
    extensions: ['.js', '.json', '*']
  },
  module: {
    rules: [{
      test: /\.html$/,
      use: [{
        loader: 'html-loader',
        options: {
          minimize: true
        }
      }]
    }]
  },
  devServer: {
    historyApiFallback: true,
    port: 9000
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'js/vendor.js'
    }),
    new html({
      filename: 'index.html',
      template: 'src/index.html'
    }),
    new live({appendScriptTag: true}),
    new hot()
  ]
};
