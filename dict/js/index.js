webpackJsonp([0],[
/* 0 */,
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function Controller() {
    var $ctrl = this;

    $ctrl.filter = {};
    $ctrl.id = 0;

    $ctrl.$onInit = $onInit;
    $ctrl.$onChange = $onChange;
    $ctrl.deleteRow = deleteRow;
    $ctrl.addRow = addRow;
    $ctrl.getCount = getCount;

    function $onInit() {
        $ctrl.storage = JSON.parse(localStorage.getItem('sml')) || [];

        if ($ctrl.storage.length) {
            $ctrl.id = $ctrl.storage[$ctrl.storage.length - 1].id;
        }
    }

    function $onChange() {
        localStorage.setItem('sml', JSON.stringify($ctrl.storage));
    }

    function getCount() {
        var count = 0;

        angular.forEach($ctrl.storage, function (item) {
            count += item.guest.coupled ? 2 : 1;
        });

        return count;
    }

    function addRow(filter) {
        $ctrl.storage.push({
            id: ++$ctrl.id,
            guest: {
                name: '',
                age: 25,
                coupled: false,
                drink: filter || 'WINE'
            }
        });

        $ctrl.$onChange();
    }

    function deleteRow(id) {
        var guestIndex = $ctrl.storage.findIndex(function (guest) {
            return guest.id === id;
        });

        $ctrl.storage.splice(guestIndex, 1);

        $ctrl.$onChange();
    }
}

/* harmony default export */ __webpack_exports__["a"] = (Controller);

/***/ }),
/* 2 */,
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__controller__ = __webpack_require__(1);


angular.module('sml', [])
    .filter('drink', function () {
        return function (array, drink) {
            var out = array;

            if (drink) {
                out = [];

                angular.forEach(array, function (item) {
                    item.guest.drink === drink && this.push(item);
                }, out);
            }

            return out;
        };
    })
    .controller('smlCtrl', __WEBPACK_IMPORTED_MODULE_0__controller__["a" /* default */]);


/***/ })
],[3]);